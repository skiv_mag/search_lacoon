from django.shortcuts import render
from django.views.generic import ListView, TemplateView
from django.views.decorators.http import require_POST
from django.http import HttpResponse

from search_engine.models import Tags


class ListTags(ListView):

    model = Tags
    template_name = 'search_engine.html'
