
from lxml.html import fromstring
import re


class AbstractEngine (object):

    def __init__(self, url_id, doc, **kwargs):
        self.url_id = url_id
        self.doc = fromstring(doc)

    def run(self):
        pass


class ImageEngine(AbstractEngine):

    def __init__(self, *args, **kwargs):
        self.max_cnt = kwargs.pop("max_cnt")
        super(ImageEngine, self).__init__(*args, **kwargs)

    def run(self):
        cnt = len([x for x in self.doc.xpath("//img/@src")])
        if cnt > 5:
            return {"lot_img"}
        return set([])


class WordsEngine(AbstractEngine):

    def __init__(self, *args, **kwargs):
        self.tags_list = set(kwargs.pop("tags_list"))
        super(WordsEngine, self).__init__(*args, **kwargs)

    def run(self):
        words_gen = (x.strip() for x in self.doc.xpath("//text()"))
        final_set = set([])
        r_set = set([])
        # works only with exact match
        #TODO: impliment more inteligent nlp solution
        re_pattern = r'\b{0}\b'

        for word in words_gen:
            for tag in self.tags_list:
                if re.match(re_pattern.format(tag), word, flags=re.IGNORECASE):
                    r_set.add(tag)
                    final_set.add(tag)
            if r_set:
                self.tags_list -= r_set
            r_set = set([])

        return final_set
