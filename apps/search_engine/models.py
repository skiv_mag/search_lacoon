from django.db import models
from jsonfield import JSONField


class SearchUrl(models.Model):

    url = models.CharField(max_length=200, unique=True)
    html = models.TextField()

    def __unicode__(self):
        return "{0}".format(self.url)


class Tags(models.Model):

    TAG_TYPE = (
        ('word', 'word'),
        ('image', 'image'),
    )

    word = models.CharField(max_length=200, unique=True)
    urls = models.ManyToManyField(SearchUrl)
    tag_type = models.CharField(max_length=100, choices=TAG_TYPE)

    def __unicode__(self):
        return "{0}".format(self.word)


# TODO: add some js form for extendble settings or migrate for some
# another solution
class EngineSettings(models.Model):

    ENG_TYPE = (
        ('word', 'word'),
        ('image', 'image'),
    )

    engine_type = models.CharField(max_length=100, choices=ENG_TYPE)
    json = JSONField()

    def __unicode__(self):
        return "{0} settings".format(self.engine_type)
