from django.conf.urls import patterns, url

from search_engine.views import ListTags

urlpatterns = patterns('search_engine.views',
    url(r'^$', ListTags.as_view(), name='tags_list'),
)