from __future__ import absolute_import

from celery import shared_task
from celery.task import periodic_task
from celery.task.schedules import crontab

import urllib
from lxml.html import fromstring, tostring
from lxml.html.clean import clean_html

from search_engine.models import SearchUrl, Tags, EngineSettings
from search_engine.engines import ImageEngine, WordsEngine


@shared_task
def run_image_engine(search_model):
    settings = EngineSettings.objects.get(engine_type='image')
    img_e = ImageEngine(
        search_model.url, search_model.html, max_cnt=settings.json["max_cnt"])
    results = img_e.run()
    if results:
        tag = Tags.objects.filter(tag_type='image').first()
        tag.urls.add(search_model)
        tag.save()


@shared_task
def run_words_engine(search_model):
    tags = Tags.objects.filter(tag_type='word').all()
    tags_list = tags.values_list('word', flat=True)
    img_e = WordsEngine(
        search_model.url, search_model.html, tags_list=tags_list)
    results = img_e.run()
    for tag in tags:
        if tag.word in results:
            tag.urls.add(search_model)
        else:
            tag.urls.remove(search_model)


@periodic_task(run_every=crontab(minute=0, hour=0))
def run_krawler():
    urls = SearchUrl.objects.all()
    for u in urls:
        content = urllib.urlopen(u.url).read()
        doc = tostring(clean_html(fromstring(content)))
        u.html = doc
        u.save()
        run_image_engine(u)
        run_words_engine(u)
